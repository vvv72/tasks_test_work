<?php

namespace App\Controllers;

use App\Models\Task;

class TaskController extends Controller
{
    public function index()
    {
        $page = $this->request->query->get('page');
        if ($page < 1) {
            $page = 1;
        }

        $request = $this->request->query->all();
        $order = [
            'name' => $request['name'],
            'email' => $request['email'],
            'status' => $request['status']
        ];

        $tasks = new Task($this->db);
        $tasksForPage = $tasks->getAll($page, $order);


        $this->renderTemplate('tasks.index', [
            'tasks' => $tasksForPage,
            'count' => $tasks->getCount(),
            'onPageCount' => count($tasksForPage),
            'pagesCount' => ceil($tasks->getCount() / $tasks->limit),
            'request' => array_merge($request, ['page' => $page]),
            'messages' => $this->session->getFlashBag()->get('info', []),
            'userId' => $this->session->get('userId')
        ]);

    }

    public function create()
    {
        $this->renderTemplate('tasks.create');
    }

    public function store()
    {
        $data = $this->request->request->all();
        $task = new Task($this->db);
        $task->create($data);
        if ($task->save()) {
            $this->session->getFlashBag()->add('info', 'Task successfully stored');
            $this->redirect('/');
        } else {
            $this->session->getFlashBag()->add('error', 'Something went wrong');
            $this->redirect('/create');
        }

    }

    public function edit()
    {
        $userId = $this->checkAuth();

        $id = $this->request->query->get('id');
        $task = (new Task($this->db))->getById($id);
        $this->renderTemplate('tasks.edit', ['task' => $task]);
    }

    public function update()
    {
        $userId = $this->checkAuth();

        $data = $this->request->request->all();
        $id = $data['id'];
        unset($data['id']);
        $task = new Task($this->db);
        if ($task->update($data, $id)) {
            $this->session->getFlashBag()->add('info', 'Task successfully updated');
            $this->redirect('/');
        } else {
            $this->session->getFlashBag()->add('error', 'Something went wrong');
            $this->redirect('/edit?id=' . $id);
        }
    }

    public function destroy()
    {
        $userId = $this->checkAuth();

        $task = new Task($this->db);
        $id = $this->request->request->get('id');
        if ($task->destroy($id)) {
            $this->session->getFlashBag()->add('info', 'Task successfully deleted');

            $this->response->setContent(json_encode(['status' => 'ok']));
            $this->response->headers->set('Content-Type', 'application/json');
            $this->response->send();
        }
    }

    private function checkAuth()
    {
        $id = $this->session->get('userId');
        if (!isset($id)) {
            $this->session->clear();
            $this->session->getFlashBag()->add('info', 'You don\'t have access');
            $this->redirect('/');
            exit();
        }
        return $id;
    }


}