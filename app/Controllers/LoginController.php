<?php


namespace App\Controllers;


class LoginController extends Controller
{
    public function index()
    {
        $this->renderTemplate('login.index', ['messages' => $this->session->getFlashBag()->get('info', [])]);
    }

    public function doLogin()
    {
        $login = $this->request->request->get('login');
        $password = $this->request->request->get('password');

        $result = $this->db->get('users', ['id', 'login'], ['login' => $login, 'password' => crypt($password, 'salt1234')]);
        if (empty($result)) {
            $this->session->getFlashBag()->add('info', 'Login failed wrong user credentials');

            $this->redirect('/login');
        } else {
            $this->session->getFlashBag()->add('info', 'You have successfully logged in');
            $this->session->set('userId', $result['id']);

            $this->redirect('/');
        }
    }

    public function doLogout()
    {
        $this->session->clear();
        $this->session->getFlashBag()->add('info', 'You have successfully logged out');

        $this->redirect('/');
    }
}