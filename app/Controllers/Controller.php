<?php


namespace App\Controllers;

use Medoo\Medoo;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;

abstract class Controller
{

    protected $request;
    /**
     * @var Response
     */
    protected $response;
    /**
     * @var Medoo
     */
    protected $db;
    /**
     * @var Environment
     */
    protected $twig;
    /**
     * @var Session
     */
    protected Session $session;

    public function __construct(Request $request, Response $response, Session $session, Medoo $db, Environment $twig)
    {
        $this->request = $request;
        $this->response = $response;
        $this->session = $session;
        $this->db = $db;
        $this->twig = $twig;
    }

    public function renderTemplate(string $templateName, array $params = [])
    {
        $tmp = $this->twig->render($templateName . '.twig', $params);
        $this->response->setContent($tmp);
        $this->response->send();
    }

    public function redirect(string $path)
    {
        $response = new RedirectResponse($path);
        $response->send();
    }

}