<?php

namespace App\Models;

use Medoo\Medoo;

abstract class Model
{
    /**
     * @var Medoo
     */
    protected $db;

    public function __construct(Medoo $db)
    {
        $this->db = $db;
    }
}