<?php

namespace App\Models;

class Task extends Model
{
    private string $tableName = 'tasks';
    private int $id;
    private string $name;
    private string $email;
    private string $description;
    private bool $status;
    public int $limit = 3;

    public function create(array $arr)
    {
        $arr['status'] = $arr['status'] ?? false;

        $this->name = $arr['name'];
        $this->email = $arr['email'];
        $this->description = $arr['description'];
        $this->status = $arr['status'];
    }

    public function update(array $arr, int $id)
    {
        $arr['status'] = $arr['status'] ?? false;
        $checkTouch = $this->db->get($this->tableName, 'touch', ['id' => $id]);
        if (!$checkTouch['touch']) {
            $arr['touch'] = $arr['touch'] ?? false;
            $descriptionNow = $this->db->get($this->tableName, 'description', ['id' => $id]);
            if ($descriptionNow !== $arr['description']) {
                $arr['touch'] = true;
            }
        }

        return $this->db->update($this->tableName, $arr, ['id' => $id])->execute();
    }

    public function save()
    {
        return $this->db->insert(
            $this->tableName,
            [
                'name' => $this->name,
                'email' => $this->email,
                'description' => $this->description,
                'status' => $this->status
            ]
        )->rowCount();
    }

    public function destroy(int $id)
    {
        return $this->db->delete($this->tableName, ['id' => $id])->rowCount();
    }

    public function getById(int $id): array
    {
        return $this->db->get($this->tableName, '*', ['id' => $id]);
    }

    public function getAll($page, $order): array
    {
        $limitOrder = [];
        if (isset($page)) {
            $limitOrder["LIMIT"] = $this->pagination($page);
        }

        $order = $this->order($order);
        if (!empty($order)) {
            $limitOrder["ORDER"] = $order;
        }

        return $this->db->select($this->tableName, "*", $limitOrder);
    }


    private function pagination($page = 1)
    {
        $page--;
        $offset = $page * $this->limit;

        return [$offset, $this->limit];
    }


    private function order($order)
    {
        return array_map('strtoupper', array_filter($order));
    }

    public function getCount(): int
    {
        return $this->db->count($this->tableName);
    }

}
