<?php
$root = dirname(__DIR__);
require_once "{$root}/bootstrap/app.php";

$stmt = $_db->create('tasks',
    [
        "id" =>
            [
                "INT",
                "NOT NULL",
                "AUTO_INCREMENT",
                "PRIMARY KEY"
            ],
        "name" =>
            [
                "VARCHAR(255)",
                "NOT NULL"
            ],
        "email" =>
            [
                "VARCHAR(255)",
                "NOT NULL"
            ],
        "description" =>
            [
                "TEXT"
            ],
        "status" =>
            [
                "TINYINT",
                "RAW" => "DEFAULT 0"
            ],
        "touch" =>
            [
                "TINYINT",
                "RAW" => "DEFAULT 0"
            ]
    ]);
var_dump($stmt->execute());

for ($i = 1; $i <= 1; $i++) {
    $_db->insert('tasks',
        [
            "name" => "test{$i}",
            "email" => "test{$i}@test.com",
            "description" => "randomrandomrandomrandom",
            "status" => $i % 2 === 0
        ]);
}

$stmt = $_db->create("users",
    [
        "id" =>
            [
                "INT",
                "NOT NULL",
                "AUTO_INCREMENT",
                "PRIMARY KEY"
            ],
        "login" =>
            [
                "VARCHAR(255)",
                "NOT NULL",
                "UNIQUE"
            ],
        "password" =>
            [
                "VARCHAR(255)",
                "NOT NULL"
            ]
    ]);
var_dump($stmt->execute());

$stmt = $_db->insert("users",
    [
        "login" => "admin",
        "password" => crypt("123", $config["salt"])
    ]);
var_dump($stmt->errorCode());

