<?php

/*
    GET	    /	              index	  tasks.index
    GET	    /create        	  create  tasks.create
    POST    /	              store	  tasks.store
//    GET	    /task?id=:num	  show	  tasks.show
    GET	    /edit?id=:num     edit	  tasks.edit
    PUT	    /task?id=:num	  update  tasks.update
    DELETE  /task?id=:num     destroy tasks.destroy
 */

//  METHOD_/route => [ Controller, Action ]

return [
    'GET_/' => ['Controller' => App\Controllers\TaskController::class, 'Action' => 'index'],
    'GET_/create' => ['Controller' => App\Controllers\TaskController::class, 'Action' => 'create'],
    'POST_/' => ['Controller' => App\Controllers\TaskController::class, 'Action' => 'store'],
//    'GET_/task' => ['Controller' => App\Controllers\TaskController::class, 'Action' => 'show'],
    'GET_/edit' => ['Controller' => App\Controllers\TaskController::class, 'Action' => 'edit'],
    'PUT_/task' => ['Controller' => App\Controllers\TaskController::class, 'Action' => 'update'],
    'DELETE_/task' => ['Controller' => App\Controllers\TaskController::class, 'Action' => 'destroy'],

    'GET_/login' => ['Controller' => App\Controllers\LoginController::class, 'Action' => 'index'],
    'POST_/login' => ['Controller' => App\Controllers\LoginController::class, 'Action' => 'doLogin'],
    'GET_/logout' => ['Controller' => App\Controllers\LoginController::class, 'Action' => 'doLogout'],
];

