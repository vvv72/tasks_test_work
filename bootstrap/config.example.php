<?php
return
    [
        'db' =>
            [
                'database_type' => 'mysql',
                'database_name' => 'test',
                'server' => 'localhost',
                'username' => 'root',
                'password' => 'secret'
            ],
        'salt' => 'salt'
    ];