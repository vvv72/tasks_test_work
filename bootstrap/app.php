<?php

$root = dirname(__DIR__);
require_once "{$root}/vendor/autoload.php";

use Medoo\Medoo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$_session = new Session();
$_session->start();

$loader = new FilesystemLoader("{$root}/app/Views");
$_twig = new Environment($loader, [
//    'cache' => "{$root}/cache",
    ['debug' => true, 'auto_reload' => true, 'cache' => false]
]);

$config = require "{$root}/bootstrap/config.php";

$_request = Request::createFromGlobals();

$_routes = require "{$root}/routes/web.php";

$_db = new Medoo([
    'database_type' => $config['db']['database_type'],
    'database_name' => $config['db']['database_name'],
    'server' => $config['db']['server'],
    'username' => $config['db']['username'],
    'password' => $config['db']['password']
]);
