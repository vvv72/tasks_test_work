<?php
require_once __DIR__ . '/../bootstrap/app.php';

use Symfony\Component\HttpFoundation\Response;

$_method = $_request->request->get('_method');
if (isset($_method)) {
    $_request->setMethod($_method);
    $_request->request->remove('_method');
}

$methPath = $_request->getMethod() . '_' . $_request->getPathInfo();

if (isset($_routes[$methPath])) {
    $_response = new Response();
    $controllerName = $_routes[$methPath]['Controller'];
    $controller = new $controllerName($_request, $_response, $_session, $_db, $_twig);
    $action = $_routes[$methPath]['Action'];
    $controller->$action();
} else {
    $response = new Response('<center style="margin-top: 50vh;">Not Found 404</center>', 404);
    $response->send();
}


